<?php
/**
 * Common Page Markup
 *
 * Option page framework for all DevSales pages
 */

?>

<div class="devsales-wrap">
	<div class="devsales-pad">
		<!-- settings and modal -->
		<?php $this->print_markup('common.modals'); ?>

		<!-- main -->
		<div class="devsales-main fix">
			<div class="devsales-main-pad">
				<?php $this->print_markup("{$this->page}.page"); ?>
			</div>
		</div>

		<!-- sidebar -->
		<div class="devsales-sb-right devsales-sb">
			<div class="devsales-sb-right-pad">
				<?php $this->print_markup("{$this->page}.sidebar"); ?>
			</div>
		</div>

		<div class="clear"></div>
		<!-- credits -->
		<div class="devsales-credits">
			<p>This epic piece of awesome-sauce is brought to you by <a href="http://twitter.com/nphaskins" target="_blank">@nphaskins</a> and <a href="http://twitter.com/aaemnnosttv" target="_blank">@aaemnnosttv</a>.<br />
			Log bugs/requests <a href="https://bitbucket.org/beardedavenger/devsales/issues?status=new&amp;status=open" target="_blank">here</a>.
			</p>
		</div>
	</div>
</div>
