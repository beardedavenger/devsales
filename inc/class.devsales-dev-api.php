<?php
/**
 * @author  Evan Mattson
*/

class DevSalesStoreDevAPI {

	const version = '1.0.3';

	private $creds;
	private $validated;
	private $products;
	private $sales;

	// Construct
	function __construct( $username = '', $apikey = '', $cache = true ) {

		$this->cache = $cache ? true : false;

		$this->api = array(
			'dataurl'  => 'http://api.pagelines.com/devs',
			'version'  => '1.1',
			'endpoint' => ''
		);
		$this->creds = array(
			'user'     => $username,
			'key'      => $apikey,
		);
		$this->validate_creds();
		$this->mode    = $this->validated ? 'authenticated' : 'public';

		$this->actions();
	}

	private function validate_creds() {
		$test = $this->api_request( 'developer-total', array('output' => 'raw') );

		if ( is_null( $test ) ) {
			$this->creds['key'] = '';
			$this->validated = false;	
		}
		else
			$this->validated = true;
	}

	function actions() {
		// Custom option
		add_action( 'pagelines_options_apikey', array(&$this, 'apikey_option'), 10, 2 );
	}

	public function request( $request, $args = array() ) {
		return $this->api_request( $request, $args );
	}

	/**
	 * Submits a request to the API
	 *
	 * @uses  $this->is_private_request() 	checks to see if the request requires validation
	 * @uses  $this->return_response() 		determines what to return on success
	 * 
	 * @param  string 	$request 	request slug
	 * @param  array  	$args    	request modifiers/options
	 * @return mixed 				array on success (see return_response method)
	 *                         		string html error messages on error
	 */
	private function api_request( $request, $args = array() ) {

		$d = array(
			'request' => $request,
			'product' => '',
			'days'    => ''
		);

		$args = wp_parse_args( $args, $d );

		// only extract keys defined in defaults
		extract( shortcode_atts( $d, $args ) );
		extract( $this->api );
		extract( $this->creds );

		$int_days = $this->get_int_days( $days );

		switch ( $request ) {
			
			case 'developer-total':
				$endpoint = sprintf('all%s', $int_days ? ":$int_days" : '');
				break;
			
			case 'product-total':
				$endpoint = sprintf('sales:%s', $int_days ? "$product,$int_days" : $product);
				break;
			
			case 'list-products':
				$endpoint = 'list';
				break;

			case 'some-public-request':
				// nothing happening here yet
				break;

			default:
				return;
		}

		$cached = $this->get_cached_request( $request, $args );

		// use cached?
		if ( $this->cache && is_array( $cached ) ) {
			$args['cached'] = true;
			return $this->return_response( $cached, $args );
		}

		//	at this point the cached data has expired
		//	or nothing has been cached for this request yet
		//  make a new request to the api
		$api_request = "$dataurl/$version";
		$api_request .= $this->is_private_request( $request ) ? "/$user/$key" : '';
		$api_request .= "/$endpoint";

		// send the request
		$response = wp_remote_get( esc_url( $api_request ) );

		return $this->return_response( $response, $args );
	}

	function get_int_days( $days ) {

		$now = current_time('timestamp');

		if ( 'mtd' == $days )
			$days = date('j', $now ); // day of month (1-31)
		elseif ( 'ytd' == $days )
			$days = date('z', $now ) + 1; // day of year (0-365) day 0 will give all products
	

		return $days;
	}

	private function return_response( $response, $args ) {

		$d = array(
			'full'    => '',
			'cached'  => '',
			'request' => '',
			'output'  => ''
		);
		extract( shortcode_atts( $d, $args ) );

		// check the response
		if ( is_wp_error( $response ) ) {

			$msg = '<p><strong>Error</strong></p>';
			foreach ( $response->errors as $errors )
				$msg .= sprintf('<p>%s</p>', implode(' ', $errors));

			return $msg;
		}
		
		$body = json_decode( $response['body'], true );

		// to cache or not to cache?
		// make sure we don't recache a cached response
		if (
			!$cached
			&& is_array( $body )
			&& isset( $response['response']['code'] )
			&& 200 === $response['response']['code']
			)
			$this->cache_response( $request, $args, $response );

		// determine what to return
		$r = $full ? $response : $body;

		if ( 'raw' == $output )
			return $r;
		
		$r['date'] = $response['headers']['date'];
		$r['timestamp'] = $this->get_timestamp( $r['date'] );
		$r['localtime'] = $this->get_timestamp( $r['timestamp'], true );

		// tag the returned data as cached 
		$r['cached'] = $cached ? true : false;

		return $r;
	}

	/**
	 * Returns a cached request
	 * Clears cache when caching is disabled
	 * 
	 * @param  string 	$request 	api request type slug
	 * @param  array 	$args    	request args
	 * @return mixed 				Value of transient. If the transient does not exist, does not have a value, or has expired, then get_transient will return false
	 */	
	private function get_cached_request( $request, $args ) {

		$key = $this->get_cached_key( $request, $args );

		if ( !$this->cache )
			delete_transient( $key );

		return get_transient( $key );
	}

	private function cache_response( $request, $args, $data ) {

		$key = $this->get_cached_key( $request, $args );

		if ( $key && $this->cache )
			set_transient( $key, $data, HOUR_IN_SECONDS*2 );
	}

	/**
	 * Assembles an option key for saving/retreiving request data based on the request
	 * 
	 * @param  string 	$request 	api request type slug
	 * @param  array 	$args    	request args
	 * @return string          		key
	 */
	private function get_cached_key( $request, $args ) {

		$user    = $this->creds['user'];
		$product = $args['product'];
		$days    = $args['days'];

		if ( 'product-total' == $request && $product )
			$request .= "_$product"; // append product slug
		
		if ( $days && intval( $days ) )
			$request .= "_{$days}-days"; // integer days
		elseif ( $days )
			$request .= "_$days"; // mtd, ytd

		return "{$user}_$request";
	}

	private function is_private_request( $request ) {
		$private = array(
			'developer-total',
			'product-total',
			'list-products'
		);
		return in_array( $request, $private );
	}

	/**
	 * Sets up the 'products' property with an array of user's product data in this format:
	 * array('product-slug' => array(
	 * 		'id'			=> 123,
	 * 		'slug'			=> 'some-product-slug',
	 * 		'name'			=> 'Real Product Name',
	 * 		'description'	=> 'blah blah blah',
	 * 		'lastmod'		=> 1359510914, // timestamp
	 * 		'price'			=> '19.95',
	 * 		'image'			=> 'http://api.pagelines.com/files/plugins/img/some-product-slug-thumb.png',
	 * 		'demo'			=> 'some demo url',
	 * 		'downloads'		=> 321,
	 * 		'type'			=> 'plugin' // plugin/section/theme
	 * 		'sales'			=> array(
	 * 			'total' => 1234,
	 * 			'items' => array(
	 * 				0 => array(
	 * 					'date' => '2013-03-07 00:00:00',
	 * 					'price' => '15.00',
	 * 					),
	 * 				1 => array() // ...	
	 * 				)
	 * 			)
	 *   	),
	 * )
	 *
	 * @uses  'list-products' 	api request type
	 * @uses  'product-total' 	api request type
	 */
	private function setup_products() {

		$user = $this->creds['user'];

		$vault = get_transient("{$user}_products");

		// check for cached
		if ( $this->cache && is_array($vault) && isset($vault['products']) ) {
			$this->products = $vault['products'];
		}
		else {
			delete_transient("{$user}_products");

			// rebuild the product table!
			$r = $this->api_request( 'list-products', array('full' => 1) );

			$products = array();

			$list = json_decode( $r['body'], true );

			if ( is_array( $list ) ) {

				foreach ( $list as $p ) {
					$p['type'] = self::get_ext_type( $p );
					$p['sales'] = $this->api_request( 'product-total', array('product' => $p['slug'], 'output' => 'raw') );
					$products[ $p['slug'] ] = $p;
				}

				$vault = array(
					'age'      => strtotime( $r['timestamp'] ),
					'products' => $products,
				);
				
				if ( $this->cache )
					set_transient( "{$user}_products", $vault, HOUR_IN_SECONDS*2 );
			}

			$this->products = $products;
		}
	}

	/**
	 * Public method for retreiving products
	 * Only makes products available to authenticated users
	 */
	public function get_products() {

		if ( !$this->validated )
			return false;

		$good = $this->maybe_setup_products();

		return $good ? $this->products : false;
	}

	/**
	 * Public method for retreiving sales
	 * Only available to authenticated users
	 */
	public function get_sales() {

		if ( !$this->validated )
			return false;

		$good = $this->maybe_setup_sales();

		return $good ? $this->sales : false;
	}

	/**
	 * Sets up the array of developer products if it hasn't been done yet
	 * @return (bool) products are setup or not!?
	 */
	private function maybe_setup_products() {
		
		if ( is_array( $this->products ) )
			return true;
		else
			$this->setup_products();
		
		return is_array( $this->products );
	}

	function filter_products( $args ) {

		$d = array(
			'sales_start_date' => '',
			'sales_end_date'   => '',
			'type' => ''
		);
		$args = wp_parse_args( $args, $d );

		if ( false !== $products = $this->get_products() )
			$all = $products;
		else
			return false; // door for those who aren't authenticated

		$filtered = array();

		foreach ( $all as $slug => $p ) {
			$_prod = $this->filter_product( $p, $args );

			if ( is_array($_prod) )
				$filtered[ $slug ] = $_prod;
		}

		return $filtered;
	}

	function filter_sales( $args ) {

		$d = array(
			'sales_start_date' => '',
			'sales_end_date'   => '',
			'type' => ''
		);
		$args = wp_parse_args( $args, $d );

		$products = $this->filter_products( $args );

		$sales = $this->get_sales_from_products( $products );

		return $sales;
	}

	function filter_product( $p, $args ) {

		$type  = str_replace( ' ', '', $args['type'] );
		$types = explode( ',', $type ); // allow multiple types to be specified comma-separated
		
		// allow singular or plural
		foreach ( $types as &$t ) {
			$t = rtrim( $t , 's' );
			$t .= 's';
		}
		// if a type(s) is specified - check to see if there is a match, otherwise bail
		if ( $type && !in_array($p['type'], $types) )
			return false;

		// if filtering sales dates, do it.
		if ( $args['sales_start_date'] || $args['sales_end_date'] )
			$p['sales'] = $this->filter_product_sales($p['sales'], $args);

		return $p;
	}

	function filter_product_sales( $sales, $args ) {

		if ( !isset( $sales['items'] ) )
			return $sales;

		$items = $sales['items'];

		// start
		if ( $args['sales_start_date'] )
			$start = $this->get_timestamp( $args['sales_start_date'] );
		else
			$start = 0;
		// end
		if ( $args['sales_end_date'] )
			$end = $this->get_timestamp( $args['sales_end_date'] );
		else
			$end = current_time( 'timestamp', true ); // gmt

		// filter sales items
		foreach ( $items as $key => $data ) {
			
			$saletime = $this->get_timestamp( $data['date'] );

			if ( $start < $saletime && $saletime < $end )
				continue; // match
			else
				unset( $items[ $key ] );
		}

		// rebuild sales
		$newtotal = 0;

		foreach ( $items as $data )
			$newtotal += $data['price'];

		$new_sales = array(
			'total' => $newtotal,
			'items' => array_values( $items ) // reindex
		);

		return $new_sales;
	}

	private function setup_sales() {

		$products = $this->get_products();
		
		$sales = $this->get_sales_from_products( $products );

		$this->sales = $sales; 
	}

	function get_sales_from_products( $products ) {
		$sales = array();
		if ( is_array( $products ) && ! empty( $products ) ) {
			foreach ( $products as $slug => $p ) {

				if ( ! isset( $p['sales']['items'] ) )
					continue;

				foreach ( $p['sales']['items'] as $sale ) {
					$time = $this->get_timestamp( $sale['date'] );
					$saledata = array(
						'datatype' => 'sale',
						'product'  => $slug,
						'price'    => $sale['price']
					);
					
					// must account for the possibility that more than 1 sale happened at the exact same time
					if ( isset( $sales[ $time ] ) )
						$sales[ $time ][] = $saledata;
					else
						$sales[ $time ] = array( $saledata );
				}
			}
		}
		return $sales;
	}

	function get_sales_from_product( $p ) {
		return $this->get_sales_from_products( array( $p ) );
	}

	private function maybe_setup_sales() {
		if ( !is_array( $this->sales ) && $this->validated )
			$this->setup_sales();
		
		return is_array( $this->sales );
	}

	function is_validated() {
		return $this->validated;
	}

	function get_timestamp( $time, $localize = false ) {
		
		if ( is_int( $time ) || ( strlen(intval($time)) == strlen($time) ) )
			$timestamp = $time;
		else {
			$timestring = (false === strpos( $time, 'GMT')) ? "$time GMT" : $time;
			$timestamp = strtotime( $timestring );
		}


		if ( $localize )
			$timestamp += ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
		
		return $timestamp;
	}

	function localize_time( $time ) {
		return $this->get_timestamp( $time, true );
	}

	function validate_product( $slug ) {
		$products = $this->get_products();
		return isset( $products[ $slug ] ) ? $slug : '';
	}

	/**
	 * Helper function for retreiving product info
	 * @param  string 	$slug 	product slug
	 * @param  string 	$key  	info key
	 * @return mixed
	 */
	function get_product_info( $slug, $key ) {
		$products = $this->get_products();
		return isset( $products[ $slug ][ $key ] ) ? $products[ $slug ][ $key ] : '';
	}

	function get_ext_type( $p ) {
		 //'image' => 'http://api.pagelines.com/files/plugins/img/page-less-thumb.png',
		preg_match( '#/files/([^/]+)#', $p['image'], $matches );

		return isset( $matches[1] ) ? $matches[1] : '';
	}

	/**
	 * Custom Option
	 * Works the same as 'text' but displays as input[type=password]
	 */
	function apikey_option( $oid, $o ) {
		echo OptEngine::input_label($o['input_id'], $o['inputlabel']);
		echo OptEngine::input_text($o['input_id'], $o['input_name'], pl_html($o['val']), 'regular-text', 'password', '', $o['placeholder'] );		
	}

} // DevSalesStoreDevAPI