<?php
/**
 * Overview Page Sidebar
 */

?>

<div id="sb_right" class="devsales-sb-unit-wrap">
	<div class="devsales-sb-unit-pad">
		<nav class="devsales-sb-content-nav" data-default-tab="mnthview">
			<a class="devsales-sb-tab-mthview" data-tab-slug="mnthview" href="#">Month in View</a>
			<a class="devsales-sb-tab-ticker" data-tab-slug="ticker" href="#">Ticker</a>
		</nav>

		<section class="devsales-sb-content-inner">
			<div id="mnthview" class="tab-content" style="display:none;">
				<ul class="devsales-list-sales fix">
					<?php $total = $this->the_sb_list(); ?>
				</ul>
				<div class="devsales-sb-totals">
					<p>total <span class="devsales-sb-total-integer"><?php echo $total; ?></span></p>
				</div>
			</div>
			<div id="ticker" class="tab-content" style="display:none;">
				<ul class="devsales-sb-ticker fix">
					<?php $this->sb_ticker();?>
				</ul>
			</div>
		</section>

	</div>
</div>