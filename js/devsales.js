/**
 * DevSales
 */

jQuery(document).ready(function( $ ) {

	// Do Tooltips
	$("[rel=tooltip]").tooltip();

	// Modals
	modal = $('#devsales-settings-modal,#devsales-settings-modal-backdrop');
	launchpadModal = $('#devsales-launchpad-modal,#devsales-settings-modal-backdrop');
	forumModal = $('#devsales-forum-modal,#devsales-settings-modal-backdrop');

	// Hide the modals (also set with display:none to avoid FOUC)
	$(modal).hide();
	$(launchpadModal).hide();
	$(forumModal).hide();

	// When Settings Icon is clicked
	$('.devsales-open-settings').click(function(e){
		e.preventDefault();
		$(modal).fadeIn('fast');
	});

	// Close the setings modal andr emove the iframe when backdrop is clicked
	$('#devsales-settings-modal-backdrop').click(function(e){
		e.preventDefault();
		$(modal).fadeOut();
		$(launchpadModal).fadeOut();
		$(forumModal).fadeOut();
		$('#frame').attr('src', '');
		$('#forumframe').attr('src', '');
	});

	// Load launchpad when icon is clicked
	// Lazy load the iframe src so its not loading all the time
	url = '//www.pagelines.com/launchpad/login.php';
	$('.devsales-open-launchpad').click(function(e){
		e.preventDefault();
		$(launchpadModal).fadeIn('fast');
		$('#frame').prop('src', url);
	});


	forumurl = '//www.pagelines.com/forum/forum/37-pagelines-store-developers/';
	$('.devsales-open-forum').click(function(e){
		e.preventDefault();
		$(forumModal).fadeIn('fast');
		$('#forumframe').prop('src', forumurl);
	});

	// Settings Tabs - Yeah its kinda dirty but didnt really expect to have more than 1 settings so whatever balls
	creds = $('#devsales-creds-tab-content');
	settings = $('#devsales-settings-tab-content');
	info = $('#devsales-info-tab-content');

	$(settings).hide();
	$(info).hide();

	$('.devsales-nav-tab-settings').click(function(e){
		e.preventDefault();
		$(settings).show();
		$(info).hide();
		$(creds).hide();
	});

	$('.devsales-nav-tab-creds').click(function(e){
		e.preventDefault();
		$(settings).hide();
		$(info).hide();
		$(creds).show();
	});

	$('.devsales-nav-tab-info').click(function(e){
		e.preventDefault();
		$(settings).hide();
		$(info).show();
		$(creds).hide();
	});


	// Sidebar Tab Nav
	$('.devsales-settings-content-nav a').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active').siblings().removeClass('active');
    });

    // Sidebar Tabs
    $sb_nav = $('#sb_right nav');
    $sb_links = $sb_nav.children();
    $sb_tabs = $('#sb_right section .tab-content');

    overview_tab = 'devsales_overview_cur_sb_tab';
    overview_last_tab = $.cookie(overview_tab);
    overview_default_tab = $sb_nav.attr('data-default-tab');
    overview_initial_tab = overview_last_tab ? overview_last_tab : overview_default_tab;

    // show initial
    $sb_links.filter('[data-tab-slug='+overview_initial_tab+']').addClass('active');
    $('#'+overview_initial_tab).fadeIn('fast');

    // change tabs
	$sb_links.click( function( e ) {
		e.preventDefault();
		$link     = $(this);
		target_id = $link.attr('data-tab-slug');
		active    = $sb_links.filter('.active');
		active_id = active.attr('data-tab-slug');

		if ( active_id != target_id ) {
			$active = $('#'+active_id);
			$active.fadeOut('fast',
				function() {
					active.removeClass('active');
					$link.addClass('active');
					$('#'+target_id).fadeIn();
				}
			);

			$.cookie(overview_tab, target_id);
		}
	});

	/**
	 * Highcharts
	 */
	Highcharts.theme = {
	   colors: ["#514F78", "#42A07B", "#9B5E4A", "#72727F", "#1F949A", "#82914E", "#86777F", "#42A07B"],
	   chart: {
	      className: 'skies',
	      borderWidth: 0,
	      plotShadow: false,
	      backgroundColor: 'transparent',
	      plotBackgroundColor: {
	         linearGradient: [0, 0, 250, 500],
	         stops: [
	            [0, 'rgba(255, 255, 255, 0.1)'],
	            [1, 'rgba(255, 255, 255, 0)']
	         ]
	      },
	      plotBorderWidth: 1
	   },
	   title: {
	      style: {
	         color: 'rgba(255,255,255,0.8)',
	         font: '16px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
	      }
	   },
	   subtitle: {
	      style: {
	         color: '#6D869F',
	         font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
	      }
	   },
	   xAxis: {
	      gridLineWidth: 0,
	      lineColor: '#C0D0E0',
	      tickColor: '#C0D0E0',
	      labels: {
	         style: {
	            color: 'rgba(255,255,255,0.8)',
	            fontWeight: 'bold'
	         }
	      },
	      title: {
	         style: {
	            color: 'rgba(255,255,255,0.8)',
	            font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
	         }
	      }
	   },
	   yAxis: {
	      alternateGridColor: 'rgba(255, 255, 255, .5)',
	      lineColor: '#C0D0E0',
	      tickColor: '#C0D0E0',
	      tickWidth: 1,
	      labels: {
	         style: {
	            color: 'rgba(255,255,255,0.8)',
	            fontWeight: 'bold'
	         }
	      },
	      title: {
	         style: {
	            color: 'rgba(255,255,255,0.8)',
	            font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
	         }
	      }
	   },
	   legend: {
	      itemStyle: {
	         font: '9pt Trebuchet MS, Verdana, sans-serif',
	         color: 'rgba(255,255,255,0.8)'
	      },
	      itemHoverStyle: {
	         color: 'white'
	      },
	      itemHiddenStyle: {
	         color: 'rgba(255,255,255,0.2)'
	      }
	   },
	   labels: {
	      style: {
	         color: '#0077cc'
	      }
	   },
	    credits: {
            enabled: false
        },
	};

	// Apply the theme
	var highchartsOptions = Highcharts.setOptions(Highcharts.theme);

	$thegraph = $('#the_graph');

	if ( $thegraph.length )
		$thegraph.highcharts( devsalesGraphData );

    // Equalize heights
    heightMatch = function() {
    	$devsales_pad = $('.devsales-pad');
    	$devsales_pad.css('min-height', 0); // clear first

    	wrap = $('#wpwrap').height();

    	$devsales_pad.css('min-height', wrap);
    }

    $(window).afterresize( heightMatch );

    // Run on load
    heightMatch();

});